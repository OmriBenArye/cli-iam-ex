#!/bin/zsh

devProfile='AdminAccessNoEKS-287169096218'
rndProfile='AdministratorAccess-417270163447'


# -----------------------------------------------------------------------------
# Roles and policies creation
# -----------------------------------------------------------------------------
createPolicy(){
        policyName=$1
        echo creating policy $1
        aws iam create-policy --policy-name $1 --policy-document $2 --profile $3 > $4
}

createRole() {
	roleName=$1
	echo creating role $1
	aws iam create-role --role-name $1 --assume-role-policy-document $2 --profile $3 > $4
}

attachPolicyToRole(){
	policyArn=$(jq .Policy.Arn $1 | tr -d '"')
	policyName=$(jq .Policy.PolicyName $1 | tr -d '"')
	echo attaching $policyName to $2
	aws iam attach-role-policy --policy-arn $policyArn --role-name $2 --profile $3
}

createRolePolicyAndAttach(){
	createPolicy $2 $3 $7 $5
	createRole $1 $4 $7 $6
	attachPolicyToRole $5 $1 $7
}

# create role that is able to assume role from another account
createRolePolicyAndAttach 'cmd-role-omri' 'cmd-policy-omri' file://policy.json file://role-trust-relation.json exports/policy-export.json exports/role-export.json $devProfile

# waiting 10 sec in order for rule2 to be able to reach role1. otherwise getting MalformedPolicyDocument error (?)
echo going to sleep for 10 sec
sleep 10 

# create another account role in rnd-tests, to be consumed by the first role
createRolePolicyAndAttach 'cmd-role-omri2' 'cmd-policy-omri2' file://policy2.json file://role-trust-relation2.json exports/policy-export2.json exports/role-export2.json $rndProfile


# -----------------------------------------------------------------------------
# Assuming roles
# -----------------------------------------------------------------------------
echo 'press enter to continue to assuming roles'
read

extractCredsAndApply() {
        accessKey=$(jq .Credentials.AccessKeyId $1 | tr -d '"')
        secretAccessKey=$(jq .Credentials.SecretAccessKey $1 | tr -d '"')
        token=$(jq .Credentials.SessionToken $1 | tr -d '"')

        export AWS_ACCESS_KEY_ID=$accessKey
        export AWS_SECRET_ACCESS_KEY="$secretAccessKey"
        export AWS_SESSION_TOKEN="$token"
}

assumeRole() {
        roleArn=$(jq .Role.Arn $1 | tr -d '"')
        echo assuming $roleArn
        aws sts assume-role --role-arn $roleArn --role-session-name $2 > $3
        extractCredsAndApply $3
        echo right now you are:
        aws sts get-caller-identity
        read
}

echo right now you are:
aws sts get-caller-identity
read

assumeRole exports/role-export.json assume1 exports/assume1.json

assumeRole exports/role-export2.json assume2 exports/assume2.json

echo trying to list s3 buckets:
aws s3 ls


# -----------------------------------------------------------------------------
# Cleanup
# -----------------------------------------------------------------------------
echo 'press enter to delete all'
read 

detachAndDelete(){
	policyArn=$(jq .Policy.Arn $1 | tr -d '"')
	roleName=$(jq .Role.RoleName $2 | tr -d '"')
	echo detaching $policyArn from $roleName
	aws iam detach-role-policy --role-name $roleName --policy-arn $policyArn --profile $3

	echo deleting role $roleName
	aws iam delete-role --role-name $roleName --profile $3

	echo deleting policy $policyArn
	aws iam delete-policy --policy-arn $policyArn --profile $3
}

detachAndDelete exports/policy-export2.json exports/role-export2.json $rndProfile

detachAndDelete exports/policy-export.json exports/role-export.json $devProfile 
